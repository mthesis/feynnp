import numpy as np

f=np.load("predicted.npz")
p=f["q"]

def statinf(q):
  return {"shape":np.array(q).shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}


ll=int(p.shape[-1])
std=np.std(p,axis=0)

dataE=np.random.normal(0,std,(1000,ll))


from ageta import *


vae=getae()

dec=vae.layers[2]


x,a=dec.predict(dataE)



np.savez_compressed("decoded",x=x,a=a)

print("done")


