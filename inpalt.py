import numpy as np
import json
import os

def write(q,fnam):
  with open(fnam,"w") as f:
    f.write(q)

data=[]

def save():
  for d in data:
    write(json.dumps(d),"datasalt/"+d["id"]+".json")

while True:
  id1=input("id part 1?\n")
  id2=input("id part 2?\n")
  lid1=len(id1)
  lid2=len(id2)
  idd=id1+"".join(["0" for i in range(8-lid1-lid2)])+id2
  print(idd)
  ret={"id":idd}
  print("Enter lines, enter nothing to stop")
  lines=[]
  while True:
    acp=input("one line::")
    if len(acp)==0:break
    
    rel={}
    
    acp=acp.replace(">","inp").replace("<","out").replace("-","anti").replace("~","anti")
    
    if "inp" in acp:rel["input"]=1
    if "out" in acp:rel["output"]=1
    if "anti" in acp:rel["anti"]=1
    acp=acp.replace("inp","").replace("out","").replace("anti","")
    acp=acp.replace("qq","quark").replace("gg","gluon").replace("ll","lepton")
    rel["type"]=acp
    
    lines.append(rel)
    print("added line",json.dumps(rel))
  ret["lines"]=lines
  print("finished entering of lines")
  print("Enter vertices, enter nothing to stop")
  vertices=[]
  while True:
    os.system("clear")
    for i,line in enumerate(lines):
      print(f"{i} :: {line}")
    acp=input("one vertice::")
    if len(acp)==0:break
    
    if len(lines)<10:
      acp=acp.replace(" ","")
      rel=[int(q) for q in acp]
    else:
      rel=[int(q) for q in acp.split(" ")]
    
    
    vertices.append(rel)
    print("added vertice",json.dumps(rel))
  ret["vertices"]=vertices
  print("finished diagram")
  print(json.dumps(ret,indent=2))
  try:
    print("all used",len(list(set(np.array(vertices).ravel())))==len(lines))
  except:
    print("all used","ndef")
  reallydo=""
  while reallydo!="y" and reallydo!="n":
    reallydo=input("press y to keep, n to delete")
    if reallydo=="y":
      data.append(ret)
      save()
      os.system("clear")
      print("saved",ret["id"])
    
    
    






