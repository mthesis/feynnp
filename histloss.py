import numpy as np
import matplotlib.pyplot as plt

import sys
fix=True
if len(sys.argv)>1:
  fix=False



f=np.load("eval.npz")
falt=np.load("evalalt.npz")

q=np.load("fids.npz")["q"]


def loss(f):

  x,p=f["x"],f["p"]


  #x=x[:,:,:p.shape[-1]]
  x=x[:,:,:p.shape[-1]]



  l=(x-p)**2

  l=np.mean(l,axis=(1,2))
  l=np.sqrt(l)

  return l

l=loss(f)
lalt=loss(falt)



ln=[]
qn=[]

for i,ll in enumerate(l):
  if q[i][0]=="4" or fix:
    ln.append(ll)
    qn.append(q[i])

c=0

for ll,qq in zip(ln,qn):
  if ll>0.1:
    c+=1
    print(qq,ll)
  else:
    print("      ",qq,ll)

print(c,len(ln),c/len(ln))
print("maxima",qn[np.argmax(ln)])
print("minima",qn[np.argmin(ln)])

plt.hist(ln,bins=20)

print(lalt)


plt.plot(lalt,np.arange(len(lalt)),"o")


plt.show()


