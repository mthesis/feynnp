import numpy as np
import matplotlib.pyplot as plt
import os
from os.path import isfile
import json



dias=["datasalt/"+q for q in os.listdir("datasalt/") if isfile("datasalt/"+q)]
fids=[q for q in os.listdir("datasalt/") if isfile("datasalt/"+q)]


np.savez_compressed("fidsalt_1",q=fids)

#exit()


traf={"e":"lepton","yy":"y","z":"Z","q":"quark"}

mp={"gluon":0,"quark":1,"lepton":2,"mu":3,"H":4,"W":5,"Z":6,"y":7,"p":8,"jet":9}


def vectorfromparticle(q):
    ret=np.zeros(len(mp.keys())+4)
    ret[mp[q["type"]]]=1
    if "anti" in q.keys():ret[-4]=1
    if "input" in q.keys():ret[-3]=1
    if "output" in q.keys():ret[-2]=1
    ret[-1]=1
    return ret


def getgra(q):
    x=[]
    for line in q["lines"]:
        if line["type"] in traf.keys():line["type"]=traf[line["type"]]
        if not line["type"] in mp.keys():
            print("failed at",line)
            exit()
        x.append(vectorfromparticle(line))
    a=np.zeros((len(x),len(x)))
    for v in q["vertices"]:
        for i1,ia in enumerate(v):
            for i2,ib in enumerate(v):
                if i1==i2:continue
                a[ia,ib]=1
    return x,a






X,A=[],[]

for dia in dias:
    for i in range(1):
        with open(dia,"r") as f:
            aX,aA=getgra(json.loads(f.read()))
            X.append(aX)
            A.append(aA)
np.savez_compressed("dataalt.npz",X=X,A=A)


