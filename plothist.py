import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

addon=""
if len(sys.argv)>1:
  if len(sys.argv[1])>0:
    addon="_"+sys.argv[1]

quiet=False
if len(sys.argv)>2:quiet=sys.argv[2]=="quiet"

index=-1
try:
  if len(sys.argv)>3:index=int(sys.argv[3])
except:
  from vload import mindex
  index=mindex()
  print(f"using mindex {index}")

addstr=""
if index>-1:addstr=f"multi/{index}/"


with open(addstr+"history"+addon+".csv","r") as ff:
  c=csv.reader(ff,delimiter=",")
  jumped=False
  q=[]
  qn=[]
  epoch=[]
  for row in c:
    if not jumped:
      for e in row:
        q.append([])
        qn.append(e)
      jumped=True
      continue

    for i in range(len(row)):
      q[i].append(float(row[i]))


for i in range(1,len(q)):
  if qn[i]=="lr":
    q[i]=np.log(q[i])
    q[i]-=np.mean(q[i])
    q[i]/=np.std(q[i])*4
    q[i]+=0.75
  plt.plot(q[0],q[i],label=qn[i])
  print(qn[i],":",np.min(q[i]),np.max(q[i]))

plt.xlabel(qn[0])
plt.legend()


plt.savefig("history"+addon+".png",format="png")
plt.savefig("history"+addon+".pdf",format="pdf")

if not quiet:plt.show()

