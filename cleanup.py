import numpy as np
import matplotlib.pyplot as plt


f=np.load("data.npz",allow_pickle=True)

x=f["X"]
a=f["A"]


l=[len(ax) for ax in x]



s=np.max(l)
s=9

#print(x[0].shape)
#print(np.zeros((s-len(x[0]),x[)))


fids=np.load("fids_1.npz")["q"]

fids=np.array([q for q,ax in zip(fids,x) if len(ax)<=s])

np.savez_compressed("fids",q=fids)


xx=np.array([np.concatenate((ax,np.zeros((s-len(ax),np.array(ax).shape[-1]))),axis=0) for ax in x if len(ax)<=s])

print(xx.shape,x.shape)

print(xx.shape)
print(fids.shape)

print(a[0].shape)
print(s-len(a[0]),len(a[0]))


aq=[]
print("start")
for aa in a:
    la=len(aa)
    if la>s:continue
    ap=np.zeros((s-len(aa),len(aa)))
    aa=np.concatenate((aa,ap),axis=0)
    ap=np.zeros((len(aa),s-la))
    aa=np.concatenate((aa,ap),axis=1)
    aq.append(aa)
aq=np.array(aq)


print(aq.shape)



np.savez_compressed("data_1.npz",x=xx,a=aq)
