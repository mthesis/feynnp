import numpy as np
import matplotlib.pyplot as plt

alpha=0.01

def drawgra(A,x):
  A=np.abs(A)
  lx=len([1 for q in x[:,-1] if q>0.5])#fixed border
  A=A[:lx,:lx]
  x=x[:lx]
  phi=[i*2*np.pi/10 for i in range(lx)]
  xs=np.cos(phi)
  ys=np.sin(phi)
  
  inns=np.where(x[:,-3]>0.5)
  outs=np.where(x[:,-2]>0.5)
  
  
  for i in range(lx):
    for j in range(lx):
      if i<=j:continue
      if A[i,j]<alpha:continue
      plt.plot([xs[i],xs[j]],[ys[i],ys[j]],color="black")
      
  plt.plot(xs,ys,"o",alpha=0.5,color="black",markersize=20)
  plt.plot(xs[inns],ys[inns],"o",alpha=0.5,color="green",markersize=20)
  plt.plot(xs[outs],ys[outs],"o",alpha=0.5,color="red",markersize=20)

  
  




if __name__=="__main__":
  # f=np.load("eval.npz")
  # p=f["p"]
  # A=f["A"]
  f=np.load("decoded.npz")
  p=f["x"]
  A=f["a"]
  
  
  
  i=np.random.randint(len(p))
  
  drawgra(A[i],p[i])
  plt.show()
  
  print(f.files)

