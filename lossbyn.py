import numpy as np
import matplotlib.pyplot as plt

import sys
fix=True
if len(sys.argv)>1:
  fix=False



f=np.load("eval.npz")

falt=np.load("evalalt.npz")

q=np.load("fids.npz")["q"]

def loss(f):

  x,p=f["x"],f["p"]


  x=x[:,:,:p.shape[-1]]


  l=(x-p)**2

  l=np.mean(l,axis=(1,2))
  l=np.sqrt(l)
  
  return l,x

l,x=loss(f)
lalt,xalt=loss(falt)


ln=[]
qn=[]
xn=[]


for i,ll in enumerate(l):
  if q[i][0]=="4" or fix:
    ln.append(ll)
    qn.append(q[i])
    xn.append(x[i])


def getn(q):
  return np.sum([x[-1] for x in q])

n=[getn(q) for q in xn]


def splitana(n,ln,qn):
  q,l={},{}
  for xn,xln,xqn in zip(n,ln,qn):
    if not xn in q.keys():
      q[xn]=[]
      l[xn]=[]
    q[xn].append(xqn)
    l[xn].append(xln)
  return q,l

sq,sl=splitana(n,ln,qn)

nalt=[getn(q) for q in xalt]
# _,slalt=splitana(nalt,lalt,np.zeros_like(lalt))



for key in sq.keys():
  print(key,"minima",sq[key][np.argmin(sl[key])],"maxima",sq[key][np.argmax(sl[key])],"len",len(sq[key]))


plt.plot(n,ln,"o",alpha=0.5)
plt.plot(nalt,lalt,"o",alpha=0.5)



plt.show()








