import numpy as np
from os import system


todoo=[]

todoo.append("rm logs/o*")
todoo.append("rm models/w*")
todoo.append("rm imgs/*.png")
todoo.append("rm imgs/*.pdf")
todoo.append("rm *.csv")
todoo.append("rm *.png")
todoo.append("rm *.pdf")
todoo.append("rm job.sh")
todoo.append("rm *.json")
todoo.append("rm *.h5")
todoo.append("rm *.npz")
todoo.append("rm __pycache__ -r")
todoo.append("rm *tf*")
todoo.append("rm checkpoint")
todoo.append("rm vae/v*")
todoo.append("rm models/checkpoint")
todoo.append("rm amodels/*.tf*")
todoo.append("rm aeval/*.npz*")
todoo.append("rm data/*.npz")
todoo.append("rm multi -r")

for t in todoo:
  print("running",t)
  system(t)












